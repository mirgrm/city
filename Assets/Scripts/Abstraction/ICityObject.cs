﻿namespace City {
	public interface ICityObject : System.IDisposable {
		int SizeX { get; }
		int SizeZ { get; }
	}
}
