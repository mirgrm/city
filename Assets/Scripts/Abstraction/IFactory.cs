﻿namespace City {
	public interface IFactory {
		T Create<T>();
	}
}
