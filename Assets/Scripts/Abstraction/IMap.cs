﻿using System.Collections.Generic;

namespace City {
	public interface IMap {
		void Clear();
		void Draw();
		CellType[,] Cells { get; set; }
		CellType this[int x, int z] { get; set; }
		List<Building> Buildings { get; }
		List<(int x, int z)> Characters { get; }
	}

	public enum CellType {
		Grass,
		Road,
		Footpath,
		Building
	}

	public class Building {
		public int Index;
		public int Angle;
		public int X;
		public int Z;
	}

}
