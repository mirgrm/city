﻿namespace City {
	public interface IMapGenerator {
		IMap Generate(int sizeX, int sizeZ);
	}
}
