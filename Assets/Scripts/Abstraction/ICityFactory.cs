﻿using System;

namespace City {
	public interface ICityFactory {
		ICityObject CreateBuilding(int x, int z, int index, int angle);
		ITile CreateFootpath(int x, int z);
		ITile CreateGrass(int x, int z);
		ITile CreateRoad(int x, int z);
		ICityObject[] GetAlllBuildingsType();
		ICharacter CreateCharacter(int x, int z);
	}
}
