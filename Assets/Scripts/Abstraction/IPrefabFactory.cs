﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace City {
	public interface IPrefabFactory {
		T InstantiatePrefab<T>(T prefab, Vector3 position, Quaternion rotation, Transform parentTransform) where T : Component;
		T InstantiatePrefab<T>(T prefab, Transform parentTransform) where T : Component;
	}
}
