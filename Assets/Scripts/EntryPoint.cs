﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace City {
	public class EntryPoint : MonoBehaviour {

		[SerializeField] [ResourceLink(typeof(TextAsset))] string _configPath = "GameConfig";
		[SerializeField] [ResourceLink(typeof(ResourcesRoster))] string _resourcesRosterPath = "ResourcesRoster";
		[SerializeField] RectTransform _rootTransorm;

		void Awake() {
			var config = Newtonsoft.Json.JsonConvert.DeserializeObject<Config>(Resources.Load<TextAsset>(_configPath).text);
			var resourcesRoster = Resources.Load<ResourcesRoster>(_resourcesRosterPath);

			var gameContainer = new DiContainer();
			Installer.GameInstaller.Install(gameContainer, config, resourcesRoster);

			var prefabFactory = gameContainer.Resolve<IPrefabFactory>();
			prefabFactory.InstantiatePrefab(resourcesRoster.GenerateMapButton, _rootTransorm);
		}
	}
}
