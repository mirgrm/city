﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace City.View {
	public class Tile : MonoBehaviour, ITile {
		public void Dispose() {
			Destroy(this.gameObject);
		}
	}
}
