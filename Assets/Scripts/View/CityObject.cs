﻿using UnityEngine;
using Zenject;

namespace City.View {
	public class CityObject : MonoBehaviour, ICityObject {
		public int SizeX;
		public int SizeZ;

		[Inject] Config _config;

		int ICityObject.SizeX => SizeX;

		int ICityObject.SizeZ => SizeZ;

		public void Dispose() {
			Destroy(this.gameObject);
		}

		private void OnDrawGizmosSelected() {
			Gizmos.DrawWireCube(transform.position 
				+ Vector3.up * (_config.CellSize * 0.5f), new Vector3(SizeX * _config.CellSize, _config.CellSize, SizeZ * _config.CellSize));
		}
	}
}
