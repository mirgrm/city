﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace City.View {
	class CityFactory : ICityFactory {

		private Config _config;
		private ResourcesRoster _resourcesRoster;
		private IPrefabFactory _prefabFactory;
		private Transform _root;

		public CityFactory(Config config, ResourcesRoster resourcesRoster, IPrefabFactory prefabFactory) {
			_config = config;
			_resourcesRoster = resourcesRoster;
			_prefabFactory = prefabFactory;
			_root = new GameObject("City").transform;
		}

		public ICityObject CreateBuilding(int x, int z, int index, int angle) {
			var rotation = Quaternion.Euler(0f, angle, 0f);
			var prefab = _resourcesRoster.Buildings[index];
			var pos1 = new Vector3(_config.CellSize * x, 0f, _config.CellSize * z);
			var pos2 =
				(angle == 90 || angle == 270)
				? new Vector3(_config.CellSize * (prefab.SizeZ * 0.5f - 0.5f), 0f, _config.CellSize * (prefab.SizeX * 0.5f - 0.5f))
				: new Vector3(_config.CellSize * (prefab.SizeX * 0.5f - 0.5f), 0f, _config.CellSize * (prefab.SizeZ * 0.5f - 0.5f));
			return _prefabFactory.InstantiatePrefab(prefab, pos1 + pos2, rotation, _root).GetComponent<ICityObject>();
		}

		T CreateTile<T>(T prefab, int x, int z) where T:Component {
			var pos = new Vector3(_config.CellSize * x, 0f, _config.CellSize * z);
			return _prefabFactory.InstantiatePrefab(prefab, pos, Quaternion.identity, _root);
		}

		public ITile CreateFootpath(int x, int z) {
			return CreateTile(_resourcesRoster.Footpath, x, z);
		}

		public ITile CreateGrass(int x, int z) {
			return CreateTile(_resourcesRoster.Grass, x, z);
		}

		public ITile CreateRoad(int x, int z) {
			return CreateTile(_resourcesRoster.Road, x, z);
		}

		public ICharacter CreateCharacter(int x, int z) {
			return CreateTile(_resourcesRoster.Characters[UnityEngine.Random.Range(0, _resourcesRoster.Characters.Length)], x, z);
		}

		public ICityObject[] GetAlllBuildingsType() {
			return _resourcesRoster.Buildings;
		}
	}
}
