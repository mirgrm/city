﻿using UnityEngine;

namespace City.View {
	public class Character : MonoBehaviour, ICharacter {
		public void Dispose() {
			Destroy(this.gameObject);
		}
	}
}