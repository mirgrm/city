﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace City {
	public class ResourceLinkAttribute : PropertyAttribute {
		public Type ResourceType { get; }
		public ResourceLinkAttribute(System.Type resourceType) {
			ResourceType = resourceType;
		}
	}

	#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(ResourceLinkAttribute))]
	public class ResourceLinkDrawer : PropertyDrawer {
		protected virtual ResourceLinkAttribute Atribute {
			get {
				return (ResourceLinkAttribute)attribute;
			}
		}

		public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label) {
			var pos1 = new Rect(rect.position, new Vector2(rect.width * 0.75f, rect.height));
			var pos2 = new Rect(pos1.position + new Vector2(pos1.width, 0f), new Vector2(rect.width * 0.25f, rect.height));

			var newValue = EditorGUI.TextField(pos1, label, property.stringValue);
			var obj = Resources.Load(newValue, Atribute.ResourceType);
			var objFromField = EditorGUI.ObjectField(pos2, obj, Atribute.ResourceType, false);
			if (objFromField != obj) {
				if (objFromField != null) {
					var path = AssetDatabase.GetAssetPath(objFromField);
					// example: "Assets/Resources/config.json";
					const string resourcesDir = "/Resources/";
					var endOfResourcesDir = path.IndexOf(resourcesDir) + resourcesDir.Length;
					var fileInResources = path.Substring(endOfResourcesDir);
					var lastDotIndex = fileInResources.LastIndexOf(".");
					var resourceName = fileInResources.Substring(0, lastDotIndex);
					property.stringValue = resourceName;
				}
			} else {
				property.stringValue = newValue;
			}
		}
	}
	#endif
}
