﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace City.Model {
	public class MapGenerator : IMapGenerator {

		IFactory _factory;
		private ICityFactory _cityFactory;

		public MapGenerator(IFactory factory, ICityFactory cityFactory) {
			_factory = factory;
			_cityFactory = cityFactory;
		}

		public IMap Generate(int sizeX, int sizeZ) {
			var map = _factory.Create<IMap>();
			map.Cells = new CellType[sizeX, sizeZ];
			GenerateMainRoad(map);
			var allFootpath = new List<(int x, int z)>();
			GenerateFootpath(map, allFootpath);
			RandomFootpathList(allFootpath);
			BuildingGenerator(map, allFootpath);
			GenerateCharacters(map, allFootpath);
			return map;
		}

		void GenerateMainRoad(IMap map) {
			var sizeX = map.Cells.GetLength(0);
			var sizeZ = map.Cells.GetLength(1);
			var minSize = _cityFactory.GetAlllBuildingsType().Min(q => Mathf.Min(q.SizeX, q.SizeZ))+2;
			var x = sizeX / 4;
			var z = 0;
			bool direction = false;
			var straightIndex = 0;
			while (x < sizeX && z < sizeZ) {
				map[x, z] = CellType.Road;
				if (direction) {
					map[x, z - 1] = CellType.Road;
				} else {
					map[x - 1, z] = CellType.Road;
				}
				if (straightIndex > minSize * 2 && x < sizeX-5 && z < sizeZ-5 && Random.Range(0, 100) < 40 + straightIndex) {
					direction = !direction;
					straightIndex = 0;
					GenerateBranchRoad(map, x, z, !direction);
				} else {
					if (straightIndex > minSize && Random.Range(0, 100) > 95) {
						GenerateBranchRoad(map, x, z, !direction);
						straightIndex = 0;
					}
				}
				straightIndex++;
				if (direction) x++; else z++;
			}
		}

		void GenerateBranchRoad(IMap map, int startX, int startZ, bool direction) {
			var sizeX = map.Cells.GetLength(0);
			var sizeZ = map.Cells.GetLength(1);
			var x = startX;
			var z = startZ;

			if (Random.Range(0,100)> 50)
			while (x < sizeX - 3 && z < sizeZ - 3) {
				map[x, z] = CellType.Road;
				if (direction) {
					map[x, z - 1] = CellType.Road;
				} else {
					map[x - 1, z] = CellType.Road;
				}
				if (direction) x++; else z++;
			}

			if (Random.Range(0, 100) > 50)
			while (x > 3 && z > 3) {
				map[x, z] = CellType.Road;
				if (direction) {
					map[x, z - 1] = CellType.Road;
				} else {
					map[x - 1, z] = CellType.Road;
				}
				if (direction) x--; else z--;
			}
		}

		void GenerateFootpath(IMap map, List<(int x, int z)> allFootpath) {
			var sizeX = map.Cells.GetLength(0);
			var sizeZ = map.Cells.GetLength(1);
			for (var x=1; x< sizeX-1; x++) {
				for (var z = 1; z < sizeZ-1; z++) {
					if (map[x, z] == CellType.Road) {
						for (var xx = x - 1; xx <= x + 1; xx++) {
							for (var zz = z - 1; zz <= z + 1; zz++) {
								if (map[xx, zz] == CellType.Grass) {
									map[xx, zz] = CellType.Footpath;
									allFootpath.Add((xx, zz));
								}
							}
						}
					}
				}
			}
		}

		void GenerateCharacters(IMap map, List<(int x, int z)> allFootpath) {
			foreach (var cell in allFootpath) {
				if (Random.Range(0, 100)>30) {
					map.Characters.Add(cell);
				}
			}
		}

		void RandomFootpathList(List<(int x, int z)> allFootpath) {
			for (var i=0; i< allFootpath.Count; i++) {
				var pi = allFootpath[i];
				var randomIndex = Random.Range(0, allFootpath.Count);
				var pR = allFootpath[randomIndex];
				allFootpath[i] = pR;
				allFootpath[randomIndex] = pi;
			}
		}

		void BuildingGenerator(IMap map, List<(int x, int z)> allFootpath) {
			var allBuildings = _cityFactory.GetAlllBuildingsType();
			for (var startIndex = 0; startIndex < allBuildings.Length; startIndex++) {
				var buildingIndex = startIndex;
				foreach (var footpath in allFootpath) {
					buildingIndex = (buildingIndex + 1) % allBuildings.Length;
					var building = allBuildings[buildingIndex];
					TryCreateBuilding(map, footpath, building, buildingIndex);
				}
			}
		}

		void TryCreateBuilding(IMap map, (int x, int z) footpath, ICityObject building, int buildingIndex) {
			var sizeX = map.Cells.GetLength(0);
			var sizeZ = map.Cells.GetLength(1);
			if (footpath.x < 1 || footpath.x >= sizeX-1 || footpath.z < 1 || footpath.z >= sizeX-1) {
				return;
			}
			TryBuildingFromAngle(map, footpath.x + 1, footpath.z, building.SizeZ, building.SizeX, buildingIndex, 270);
			TryBuildingFromAngle(map, footpath.x, footpath.z +1, building.SizeX, building.SizeZ, buildingIndex, 180);

			TryBuildingFromAngleReverse(map, footpath.x - 1, footpath.z, building.SizeZ, building.SizeX, buildingIndex, 90);
			TryBuildingFromAngleReverse(map, footpath.x, footpath.z - 1, building.SizeX, building.SizeZ, buildingIndex, 0);
		}

		void TryBuildingFromAngle(IMap map, int x, int z, int sizeX, int sizeZ, int buildingIndex, int angle) {
			if (map[x, z] == CellType.Grass) {
				if (IsClear(map, x, z, sizeX, sizeZ)) {
					Fill(map, x, z, sizeX, sizeZ, CellType.Building);
					map.Buildings.Add(new Building() {
						Angle = angle,
						Index = buildingIndex,
						X = x,
						Z = z
					});
				}
			}
		}

		void TryBuildingFromAngleReverse(IMap map, int x, int z, int sizeX, int sizeZ, int buildingIndex, int angle) {
			if (map[x, z] == CellType.Grass) {
				if (IsClear(map, x - sizeX+1, z - sizeZ+1, sizeX, sizeZ)) {
					Fill(map, x - sizeX+1, z - sizeZ+1, sizeX, sizeZ, CellType.Building);
					map.Buildings.Add(new Building() {
						Angle = angle,
						Index = buildingIndex,
						X = x - sizeX + 1,
						Z = z - sizeZ + 1
					});
				}
			}
		}

		bool IsClear(IMap map, int pX, int pY, int width, int height) {
			var sizeX = map.Cells.GetLength(0);
			var sizeZ = map.Cells.GetLength(1);
			if (pX + width >= sizeX || pY + height >= sizeZ || pX < 1 || pY < 1) {
				return false;
			}
			for (var x = pX; x < pX + width; x++) {
				for (var y = pY; y < pY + height; y++) {
					if (map[x, y]!=CellType.Grass) {
						return false;
					}
				}
			}
			return true;
		}

		void Fill(IMap map, int pX, int pY, int width, int height, CellType cell) {
			for (var x = pX; x < pX + width; x++) {
				for (var y = pY; y < pY + height; y++) {
					map[x, y] = cell;
				}
			}
		}
	}
}
