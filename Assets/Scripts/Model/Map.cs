﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace City.Model {
	public class Map : IMap {


		ICityFactory _cityFactory;
		List<System.IDisposable> _allTiles = new List<System.IDisposable>(); 

		public Map(ICityFactory cityFactory) {
			_cityFactory = cityFactory;
		}

		public CellType this[int x, int z] { get => Cells[x,z]; set => Cells[x, z] = value; }

		public CellType[,] Cells { get ; set; }
		public List<Building> Buildings { get; private set; } = new List<Building>();

		public List<(int x, int z)> Characters { get; private set; } = new List<(int x, int z)>();

		public void Clear() {
			foreach (var tile in _allTiles) {
				tile.Dispose();
			}
			_allTiles.Clear();
			Buildings.Clear();
			Characters.Clear();
		}

		public void Draw() {
			for (var x = 0; x < Cells.GetLength(0); x++) {
				for (var z = 0; z < Cells.GetLength(1); z++) {
					switch (Cells[x, z]) {
						case CellType.Grass:
							_allTiles.Add(_cityFactory.CreateGrass(x, z));
							break;
						case CellType.Road:
							_allTiles.Add(_cityFactory.CreateRoad(x, z));
							break;
						case CellType.Footpath:
							_allTiles.Add(_cityFactory.CreateFootpath(x, z));
							break;
						case CellType.Building:
							//Тут надо будет разместить участки с домами
							break;
						default:
							break;
					}
				}
			}

			foreach (var building in Buildings) {
				_allTiles.Add(_cityFactory.CreateBuilding(building.X, building.Z, building.Index, building.Angle));
			}

			foreach (var character in Characters) {
				_allTiles.Add(_cityFactory.CreateCharacter(character.x, character.z));
			}
			
		}
	}
}
