﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace City {

	[CreateAssetMenu(menuName = "City/ResourcesRoster")]
	public class ResourcesRoster : ScriptableObject {
		public View.CityObject[] Buildings;
		public View.Tile Footpath;
		public View.Tile Grass;
		public View.Tile Road;
		public View.Character[] Characters;

		public GUI.GenerateMapButton GenerateMapButton;
	}
}
