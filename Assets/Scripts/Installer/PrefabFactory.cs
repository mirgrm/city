﻿using UnityEngine;
using Zenject;

namespace City.Installer {
	class PrefabFactory : IPrefabFactory {

		private IInstantiator _container;

		public PrefabFactory(IInstantiator container) {
			_container = container;
		}

		public T InstantiatePrefab<T>(T prefab, Vector3 position, Quaternion rotation, Transform parentTransform) where T : Component {
			return _container.InstantiatePrefab(prefab, position, rotation, parentTransform).GetComponent<T>();
		}

		public T InstantiatePrefab<T>(T prefab, Transform parentTransform) where T : Component {
			return _container.InstantiatePrefab(prefab, parentTransform).GetComponent<T>();
		}
	}
}
