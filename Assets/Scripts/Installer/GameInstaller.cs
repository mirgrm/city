﻿using UnityEngine;
using Zenject;

namespace City.Installer {
	public class GameInstaller : Installer<Config, ResourcesRoster, GameInstaller> {
		private Config _config;
		private ResourcesRoster _resourcesRoster;

		public GameInstaller(Config config, ResourcesRoster resourcesRoster) {
			_config = config;
			_resourcesRoster = resourcesRoster;
		}

		public override void InstallBindings() {
			Container
				.RegisterInstance(_config)
				.RegisterInstance(_resourcesRoster)
				.RegisterType<IPrefabFactory, Installer.PrefabFactory>()
				.RegisterType<IFactory, Installer.Factory>()
				.RegisterType<IMapGenerator, Model.MapGenerator>()
				.RegisterType<IMap, Model.Map>()
				.RegisterSingleton<ICityFactory, View.CityFactory>();
		}
	}
}
