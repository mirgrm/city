﻿using UnityEngine;
using Zenject;

namespace City.Installer {
	class Factory : IFactory {
		private DiContainer _container;

		public Factory(DiContainer container) {
			_container = container;
		}

		public T Create<T>() {
			return _container.Resolve<T>();
		}
	}
}
