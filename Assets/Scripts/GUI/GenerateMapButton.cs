﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace City.GUI {

	[RequireComponent(typeof(GenerateMapButton))]
	public class GenerateMapButton : MonoBehaviour {

		[Inject] IMapGenerator _mapGenerator;
		private IMap _map;

		void Start() {
			GetComponent<Button>().onClick.AddListener(Generate);
			Generate();
		}

		void Generate() {
			if (_map!=null) {
				_map.Clear();
			}
			_map = _mapGenerator.Generate(64, 64);
			_map.Draw();
		}
	}
}
